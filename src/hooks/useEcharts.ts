import {
  Ref,
  shallowRef,
  unref,
  onMounted,
  onDeactivated,
  onBeforeUnmount,
} from "vue";

import echarts from "@/components/baseEcharts/config.ts";
// import echarts from "../components/baseEcharts/config";

export type EChartsCoreOption = echarts.EChartsCoreOption;

// HTMLDivElement 指明了这个引用应该是一个指向HTML中 <div> 元素的引用  setOption 更新图表配置
const useEcharts = (elRef: Ref<HTMLDivElement>, options: EChartsCoreOption) => {
  const charts = shallowRef<echarts.ECharts>();

  const setOptions = (options: EChartsCoreOption) => {
    charts.value && charts.value.setOption(options);
  };

  // 初始化
  const initCharts = (themeColor?: Array<string>) => {
    // unref 函数通常用于从 Vue 的响应式引用中提取原始值
    const el = unref(elRef);
    if (!el) {
      return;
    }
    charts.value = echarts.init(el);
    if (themeColor) {
      options.color = themeColor;
    }
    setOptions(options);
  };

  // 重新窗口变化时，重新计算
  const resize = () => {
    console.log('Window resize event triggered');
    if (charts.value) {
      console.log('ECharts instance exists, calling resize...');
      charts.value.resize();
    } else {
      console.log('ECharts instance does not exist!');
    }
    charts.value && charts.value.resize();
  };

  onMounted(() => {
    window.addEventListener("resize", resize);
  });

  // 页面keepAlive时，不监听页面
  onDeactivated(() => {
    window.removeEventListener("resize", resize);
  });

  onBeforeUnmount(() => {
    window.removeEventListener("resize", resize);
    if (charts.value) {
      charts.value.dispose(); // 销毁ECharts实例  
    }
  });

  return {
    initCharts,
    setOptions,
    resize,
  };
};

export { useEcharts };
